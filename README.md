# Kubide Sass Architecture & Styleguide

This will be the Sass Architecture used by [Kubide](https://kubide.es/) based on [CSS Guidelines](http://cssguidelin.es/) by [Harry Roberts](https://csswizardry.com/) and [Sass Guidelines](https://sass-guidelin.es/) by [Hugo Giraudel](http://hugogiraudel.com/).

## v0.0.1 [Under Construction]

Copy and pasting from the original repositories to adapt them to Kubide's way or work.

This is a free project that [I maintain](http://ignaciodenuevo.com/) at work.

Now if you feel like contributing or fixing a tiny typo by opening an issue or a pull-request would be appreciated!

### CSS Guidelines

* [Link to our CSS Guidelines version](https://github.com/Kubide/kubide-sass-architecture-and-styleguide/blob/master/CSS-Guidelines.md)

### Sass Guidelines

* [Link to our Sass Guidelines version](https://github.com/Kubide/kubide-sass-architecture-and-styleguide/blob/master/Sass-Guidelines.md)

### Sass-Guidelines 7-1 Architecture

* [Link to our Sass 7-1 Architecture version](https://github.com/Kubide/kubide-sass-architecture-and-styleguide/tree/master/sass-boilerplate)

### Node-Only Sass Linter

* [Pure Node.js Sass linting](https://github.com/sasstools/sass-lint)

### SCSS Lint rules

* [SCSS_Lint Configuration](https://github.com/brigade/scss-lint/blob/master/lib/scss_lint/linter/README.md)

### SCSS Linter config file
* [.scss-lint.yml](https://github.com/Kubide/kubide-sass-architecture-and-styleguide/blob/master/.scss-lint.yml)

To run the linter paste `.scss-lint.yml` in the root folder and replace `./stylesheets/` with the project's  [Sass 7-1 Architecture](https://github.com/Kubide/kubide-sass-architecture-and-styleguide/tree/master/sass-boilerplate) destination folder and run the following command

```
./node_modules/.bin/sass-lint './stylesheets/**/*.scss' -v -q
```
